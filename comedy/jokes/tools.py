import csv
from jokes.models import DadJokes, Tweet, Joke
from tqdm import tqdm

def loader_csv(filename, model):
	load_file = open(filename, 'r')
	reader = csv.reader(load_file, delimiter=",")
	saved, count = 0, 0

	for row in tqdm(reader):
		joke = row[1]
		if joke != 'Joke':
			count += 1
			exists = model.objects.filter(text=joke)
			if exists:
				pass
			else:
				new = model(text=joke)
				new.save()
				saved += 1

	load_file.close()
	print ("Complete, Total Jokes {0}. {1} were saved".format(count, saved))
