from django.shortcuts import render, render_to_response

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from jokes.models import DadJokes, Tweet, Joke
from django.core.exceptions import ObjectDoesNotExist
import json

def home(request, template_name="home.html"):
	context = {'title': 'Jokes Central'}
	return render_to_response(template_name, context)

def load_model(model_name):
    models = {
        'Joke': Joke,
        'DadJokes': DadJokes,
        'Tweet': Tweet
    }

    if model_name not in models.keys():
        return Joke
    else:
        return models[model_name]

def get_jokes(model, ref_id, maximum, model_name):
    count = 0
    list_jokes = []
    last_id = model.objects.latest('id').id
    while count < maximum and ref_id <= last_id:
        try:
            joke = model.objects.get(id=ref_id)
            joke_dict = {'key': '{0}-{1}'.format(model_name, joke.id), 'text': joke.text, 'name': model_name}
            list_jokes.append(joke_dict)
            count += 1
        except ObjectDoesNotExist:
            pass

        ref_id += 1

    return list_jokes, ref_id

@csrf_exempt
def joke_list(request):
    response = {'status': None}

    if request.method == 'POST':
        data = json.loads(request.body)
        ref_id, model = data['id'], load_model(data['model-name'])
        response['jokes'], response['ref-id'] = get_jokes(model, ref_id, 10, data['model-name'])
        response['status'] = 'ok'

    else:
        response['error'] = 'no post data found'

    return HttpResponse(
            json.dumps(response),
            content_type="application/json"
        )

